# Objetivos

Realizar una aplicación completa con Laravel por fases

# Enunciado del sistema de informacion

Aplicación que me permita controlar las practicas a entregar en varios cursos . 

Queria tener los alumnos y las practicas que tienen que entregar y almacenar la nota del alumno

De los cursos quiero almacenar la siguiente información
- Id
- Nombre curso
- Fecha comienzo
- Numero de horas
- Observaciones

De las practicas necesito:
- Id
- Titulo
- Fichero
- Curso

De los alumnos necesito:
- Id
- Nombre
- Apellidos
- Fecha nacimiento
- Email (único)
- Foto
- Curso

# Diseño de la base de datos

Para empezar desarrollamos el diagrama semantico (E/R) de la aplicacion

![alt text](er.png)

# Conversion al modelo relacional

Realizamos la conversion al modelo relacional para poder implementar en un modelo de tablas.

![relacional](relacional.png)

# Instalar aplicacion base de laravel

~~~php
laravel new aplicacion7
~~~

# Instalar componentes
Instalamos todos los componentes 

~~~php
npm i 
~~~

Aunque al principio no voy a utilizar estos componentes los voy a instalar
- bootstrap
- sass
- paquete de idioma español

## Bootstrap

Para instalar bootstrap

~~~php
npm i bootstrap --save-dev
~~~

## Sass

Instalamos la opcion de utilizar preprocesamiento de css

~~~php
npm i sass --save-dev
~~~

## Traducir mensajes de validacion a castellano

Instalar componente de idiomas (colocar los mensajes de error en castellano)

~~~php
composer require laravel-lang/common --dev
php artisan lang:add es
php artisan lang:publish
~~~

# Configurar aplicacion

En el archivo .env colocamos idioma y nombre de la aplicacion

~~~php
APP_NAME=Aplicacion7
APP_ENV=local
APP_KEY=base64:l7MkG315z4Ry+mK1/MZeTSJV3rm/UbbFeSZilluG2Vk=
APP_DEBUG=true
APP_TIMEZONE=UTC
APP_URL=http://aplicacion7.test

APP_LOCALE=es
APP_FALLBACK_LOCALE=es
APP_FAKER_LOCALE=es_ES
~~~

# Modificar configuracion vite

Vamos a configurar el vite para que utilice bootstrap y css preprocesado.

Renombro el archivo resources/css/app.css a app.scss

> resources/css/app.scss
~~~php
@import 'bootstrap/scss/bootstrap';
~~~

> resources/css/app.js
~~~php
import './bootstrap';

import * as bootstrap from 'bootstrap';
~~~

Configuro el vite para el scss

> vite.config.js
~~~php
import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: ['resources/css/app.scss', 'resources/js/app.js'],
            refresh: true,
        }),
    ],
});
~~~

Cuando cree las vistas debo colocar la directiva de vite en blade

![vite](vite.png)

Recordar que cuando quiera probar la aplicacion tendre que arrancar vite en desarrollo o explotacion

![arrancar](arrancar.png)

# Creamos controladores, modelos y migraciones

Creamos los controladores, modelos, migraciones, seeder y factory de todas las tablas.

Para que no den errores en las migraciones las tablas que tienen claves ajenas (tablas relacionadas) colocarlas al final.

~~~php
php artisan make:model Alumno -mfscr
~~~

# Terminamos los archivos de migracion

Tenemos que generar la estructura de todas las tablas y relaciones de la bbdd

> Archivo \database\migrations\create_alumnos_table.php
~~~php
    public function up(): void
    {
        Schema::create('alumnos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre', 100);
            $table->string('apellidos', 200);
            $table->string('email', 100);
            $table->date('fecha_nacimiento');
            $table->string('foto', 100);
            $table->timestamps();
            $table->unique(['email'], 'email_unique');
        });
    }
~~~

Tenemos que ir colocando las claves ajenas con los campos. Por ejemplo para las practicas

> Archivo \database\migrations\create_practicas_table.php
~~~php

 public function up(): void
    {
        Schema::create('practicas', function (Blueprint $table) {
            $table->id();
            $table->string('titulo', 200);
            $table->string('fichero', 260);
            // creando la clave ajena
            $table->unsignedBigInteger('curso_id');
            $table
                ->foreign('curso_id')
                ->references('id')
                ->on('cursos')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->timestamps();
        });
    }
~~~

# Ejecutamos las migraciones
Tenemos que ejecutar las migraciones para tener las tablas creadas en el sistema gestor de base de datos.

~~~php
php artisan migrate
~~~

# Terminamos los modelos

En los modelos colocamos 
- propiedad protected para el nombre de la tabla
- propiedad protected para indicar los campos asignacion masiva (fillable)

~~~php
    // nombre de la tabla
    protected $table = 'alumnos';

    // campos de asignacion masiva
    protected $fillable = [
        'nombre', 'apellidos', 'fechanacimiento', 'email', 'foto'
    ];
~~~

- propiedad estatica labels, para colocar las etiquetas de los campos personalizadas
- metodo getAttributeLabel
- metodo getFields

~~~php
    // creamos un atributo estatico con los labels
    public static $labels = [
        'id' => 'ID',
        'nombre' => 'Nombre',
        'apellidos' => 'Apellidos',
        'fechanacimiento' => 'Fecha de Nacimiento',
        'email' => 'Email',
        'fotos' => 'Fotos'
    ];

    // metodo para devolver el label de un campo
    public function getAttributeLabel($key)
    {
        return self::$labels[$key] ?? $key;
    }

    public function getFields()
    {
        return Schema::getColumnListing($this->table);
    }
~~~

En los modelos añadimos las relaciones

~~~php
    // voy a crear las relaciones de alumno

    public function perteneces(): HasMany
    {
        return $this->hasMany(Pertenece::class);
    }
    public function presentas(): HasMany
    {
        return $this->hasMany(Presenta::class);
    }
~~~


# Factories y Seeders

Podemos crear datos de prueba para comprobar el funcionamiento de las tablas

Para ello utilizamos los seeder y los factories.

## 1 paso : Creamos los factories de cada tabla

En estos factories no coloqueis las claves ajenas

Os pongo el ejemplo de alumno

~~~php
class AlumnoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'nombre' => fake()->name(),
            'apellidos' => fake()->lastname(),
            'email' => fake()->unique()->safeEmail(),
            'fechanacimiento' => fake()->date(),
            'foto' => fake()->imageUrl(),
        ];
    }
}
~~~

Y de Practica

# Crear el layout

# Crear la vista index 

# Crear las carpetas para las vistas de cada controlador

# Comenzamos con Alumnos

## Accion index

## Vista index











