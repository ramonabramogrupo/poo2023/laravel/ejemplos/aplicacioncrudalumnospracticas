@extends('layouts.main')

@section('content')
    {{-- muestro todos los alumnos que me manda el controlador --}}
    @foreach ($alumnos as $alumno)
        <ul>
            <li>id:{{ $alumno->id }}</li>
            <li>Nombre:{{ $alumno->nombre }}</li>
            <li>Apellidos:{{ $alumno->apellidos }}</li>
            <li>Email:{{ $alumno->email }}</li>
            <li>fecha Nacimiento:{{ $alumno->fechanacimiento }}</li>
            <li>Foto: {{ $alumno->foto }}</li>
            <li>
                <a href="{{ route('alumno.show', $alumno) }}">Ver</a>
                <a href="{{ route('alumno.edit', $alumno) }}">Actualizar</a>
                <form action="{{ route('alumno.destroy', $alumno) }}" method="post" id="eliminar">
                    @csrf
                    @method('delete')
                    <button type="submit">Borrar</button>
                </form>
            </li>
        </ul>
    @endforeach
@endsection
