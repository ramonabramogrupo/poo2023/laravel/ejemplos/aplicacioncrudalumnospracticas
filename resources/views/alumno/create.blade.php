@extends('layouts.main')

@section('content')
    <form action="{{ route('alumno.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div>
            <label>Nombre</label>
            <input type="text" name="nombre">
        </div>
        <div>
            <label>Apellidos</label>
            <input type="text" name="apellidos">
        </div>
        <div>
            <label>Email</label>
            <input type="email" name="email">
        </div>
        <div>
            <label>Fecha Nacimiento</label>
            <input type="date" name="fechanacimiento">
        </div>
        <div>
            <label>Foto</label>
            <img id="preview">
            <input type="file" name="foto" id="fichero">
        </div>
        <div>
            <button type="submit">Enviar</button>
        </div>

    </form>
@endsection
@section('css')
    <style>
        .#preview {
            max-width: 100%;
            width: 200px;
        }
    </style>
@endsection
