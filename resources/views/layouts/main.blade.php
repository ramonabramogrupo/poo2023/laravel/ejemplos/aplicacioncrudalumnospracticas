<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    @vite('resources/css/app.scss')
    @yield('css')
</head>

<body>
    <div class="menu">
        <ul>
            <li><a href="{{ route('inicio') }}" class="{{ request()->routeIs('inicio') ? 'active' : '' }}">Inicio</a>
            </li>
            <li><a href="{{ route('alumno.index') }}"
                    class="{{ request()->routeIs('alumno.index') ? 'active' : '' }}">Gestion Alumnos</a></li>
            <li><a href="{{ route('alumno.create') }}"
                    class="{{ request()->routeIs('alumno.create') ? 'active' : '' }}">Crear alumno</a></li>
            <li><a href="{{ route('practica.index') }}"
                    class="{{ request()->routeIs('practica.index') ? 'active' : '' }}">Gestion practicas</a></li>
            <li>
                <a href="{{ route('practica.create') }}"
                    class="{{ request()->routeIs('practica.create') ? 'active' : '' }}">Crear practica</a>
            </li>
            <li>
                <a href="{{ route('pertenece.index') }}"
                    class="{{ request()->routeIs('pertenece.index') ? 'active' : '' }}"> Gestion Pertenece</a>
            </li>
            <li>
                <a href="{{ route('pertenece.create') }}"
                    class="{{ request()->routeIs('pertenece.create') ? 'active' : '' }}"> Crear Pertenece</a>
            </li>

        </ul>
    </div>
    <div>
        @yield('content')
    </div>
    <div class="footer">
        Aplicacion gestion de practicas y alumnos con laravel
    </div>
</body>
@vite('resources/js/app.js')
@yield('js')

</html>
