@extends('layouts.main')

@section('content')
    <div class="tarjeta">
        <form action="{{ route('practica.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div>
                <label>Titulo</label>
                <input type="text" name="titulo" required>
            </div>
            <div>
                <label>Fichero</label>
                <input type="file" name="fichero" required>
            </div>
            <div>
                <label>Curso</label>
                <select name="curso_id">
                    @foreach ($cursos as $curso)
                        <option value="{{ $curso->id }}">{{ $curso->nombre }}</option>
                    @endforeach
                </select>
            </div>
            <div>
                <button type="submit" class="boton">Enviar</button>
            </div>
        </form>
    </div>
@endSection
