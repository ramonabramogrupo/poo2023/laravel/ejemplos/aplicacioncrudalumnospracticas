@extends('layouts.main')

@section('content')
    <div class="tarjeta">
        <form action="{{ route('practica.update', $practica) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('put')
            <div>
                <label>Titulo</label>
                <input type="text" name="titulo" required value="{{ $practica->titulo }}">
            </div>
            <div>
                <label>Fichero</label>
                <div>{{ $practica->fichero }}</div>
                <input type="file" name="fichero" value="{{ $practica->fichero }}">
            </div>
            <div>
                <label>Curso</label>
                <select name="curso_id">
                    @foreach ($cursos as $curso)
                        <option value="{{ $curso->id }}" {{ $practica->curso_id == $curso->id ? 'selected' : '' }}>
                            {{ $curso->nombre }}</option>
                    @endforeach
                </select>
            </div>
            <div>
                <button type="submit" class="boton">Enviar</button>
            </div>
        </form>
    </div>
@endSection
