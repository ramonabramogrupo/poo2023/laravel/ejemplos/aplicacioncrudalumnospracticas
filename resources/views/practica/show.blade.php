@extends('layouts.main')

@section('content')
    <div class="tarjeta">
        <ul>
            <li>id:{{ $practica->id }}</li>
            <li>Titulo:{{ $practica->titulo }}</li>

            <li>Fichero: <a href="{{ asset('storage/' . $practica->fichero) }}"> {{ $practica->fichero }}</a></li>

            <li>Curso:{{ $practica->curso_id }} - {{ $practica->curso->nombre }}</li>
        </ul>
        <div class="botones">
            <a href="{{ route('practica.edit', $practica) }}" class="boton">Actualizar</a>
            <form action="{{ route('practica.destroy', $practica) }}" method="post" id="eliminar" class="form-inline">
                @csrf
                @method('delete')
                <button type="submit" class="boton">Borrar</button>
            </form>
            <a href="{{ route('practica.confirmar', $practica) }}" class="boton">Eliminar otra forma</a>
        </div>
    </div>
@endsection
