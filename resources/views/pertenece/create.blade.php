@extends('layouts.main')

@section('content')
    <div class="tarjeta">
        <form action="{{ route('pertenece.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div>
                <label>Curso</label>
                <select name="curso_id">
                    @foreach ($cursos as $curso)
                        <option value="{{ $curso->id }}">{{ $curso->nombre }}</option>
                    @endforeach
                </select>
            </div>
            <div>
                <label>Alumno</label>
                <select name="alumno_id">
                    @foreach ($alumnos as $alumno)
                        <option value="{{ $alumno->id }}">{{ $alumno->nombre }}</option>
                    @endforeach
                </select>
            </div>

            <div>
                <button type="submit" class="boton">Enviar</button>
            </div>
        </form>
    </div>
@endSection
