import './bootstrap';

import * as bootstrap from 'bootstrap';

// Eliminar registro
if (document.querySelector('#eliminar')) {
    document.querySelector('#eliminar').addEventListener('submit', (event) => {
        event.preventDefault(); //detengo el envio
        let confirmar = false;

        // mensaje emergente
        confirmar = window.confirm('¿Desea eliminar el registro?');
        if (confirmar) {
            document.querySelector('#eliminar').submit();
        }
    });
}

if (document.querySelector('#preview')) {

    document.querySelector('#fichero').addEventListener('change', (event) => {
        document.querySelector('#preview').src = window.URL.createObjectURL(event.target.files[0]);
    });
}