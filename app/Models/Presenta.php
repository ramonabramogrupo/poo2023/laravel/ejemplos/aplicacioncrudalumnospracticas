<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Schema;

class Presenta extends Model
{
    use HasFactory;

    // nombre de la tabla
    protected $table = 'presentas';

    // campos de asignacion masiva
    protected $fillable = [
        'alumno_id', 'practica_id', 'nota'
    ];

    // creamos un atributo estatico con los labels
    public static $labels = [
        'id' => 'ID',
        'alumno_id' => 'Id del Alumno',
        'practica_id' => 'Id de la Practica',
        'nota' => 'Nota',
    ];

    // metodo para devolver el label de un campo
    public function getAttributeLabel($key)
    {
        return self::$labels[$key] ?? $key;
    }

    public function getFields()
    {
        return Schema::getColumnListing($this->table);
    }

    // voy a crear las relaciones

    public function alumno(): BelongsTo
    {
        return $this->belongsTo(Alumno::class);
    }

    public function practica(): BelongsTo
    {
        return $this->belongsTo(Practica::class);
    }
}
