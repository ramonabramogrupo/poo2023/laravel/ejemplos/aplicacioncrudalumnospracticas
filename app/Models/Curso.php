<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Schema;

class Curso extends Model
{
    use HasFactory;

    // nombre de la tabla
    protected $table = 'cursos';

    // campos de asignacion masiva
    protected $fillable = [
        'nombre', 'duracion', 'fechacomienzo', 'observaciones'
    ];

    // creamos un atributo estatico con los labels
    public static $labels = [
        'id' => 'ID',
        'nombre' => 'Nombre del curso',
        'duracion' => 'Duración del curso',
        'fechacomienzo' => 'Fecha de comienzo',
        'observaciones' => 'Observaciones',
    ];

    // metodo para devolver el label de un campo
    public function getAttributeLabel($key)
    {
        return self::$labels[$key] ?? $key;
    }

    public function getFields()
    {
        return Schema::getColumnListing($this->table);
    }

    // voy a crear las relaciones

    public function practicas(): HasMany
    {
        return $this->hasMany(Practica::class);
    }

    public function perteneces(): HasMany
    {
        return $this->hasMany(Pertenece::class);
    }
}
