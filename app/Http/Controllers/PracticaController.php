<?php

namespace App\Http\Controllers;

use App\Models\Curso;
use App\Models\Practica;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PracticaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $practicas = Practica::all();

        return view(
            'practica.index',
            compact('practicas')
        );
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        // esto es para mostrar un dropdown en curso_id
        $cursos = Curso::all();

        return view(
            'practica.create',
            compact('cursos')
        );
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        // quiero crear un archivo de texto
        // y colocar hola clase
        // quiero almacenarlo en storage

        // este archivo lo crea en el disk default
        // este disco es local ==> /storage/app
        // $fichero = Storage::put('hola.txt', 'hola clase');

        // quiero un archivo que se cree en el disco public
        // el disco public ==> /storage/app/public

        // $fichero = Storage::disk('public')->put('hola.txt', 'hola clase');

        // quiero crear un archivo en el disco nuevo llamado subidas
        // este disco le he creado en el config/filesystems.php
        // subidas ==> /public/subidas
        // $fichero = Storage::disk('subidas')->put('hola.txt', 'hola clase');

        $ficheroEnviado = $request->file('fichero');
        $nombreOriginal = $ficheroEnviado->getClientOriginalName();


        // quiero almacenar en el disco public dentro de una carpeta llamadas 
        // ficheros el fichero subido


        // store(nombreCarpeta,disco) ==> lo que realiza es una copia del archivo 
        // enviado a la carpeta del disco
        // el nombre del archivo lo genera automaticamente
        $fichero = $ficheroEnviado->store('ficheros', 'public');

        // storeAs('carpeta', 'nombreOriginal', 'disco')
        // $fichero = $ficheroEnviado->storeAs('ficheros', $nombreOriginal, 'public');

        // esto lo realizaria en el disco por defecto
        // como es storage/app
        // $fichero = $ficheroEnviado->store('ficheros');


        // esto me genera el registro en la base de datos
        // automaticamente
        //$practica = Practica::create($request->all());

        // opcion 1
        // $practica = new Practica($request->all());
        // $practica->fichero = $fichero;
        // $practica->save();


        // opcion 2
        $practica = new Practica();
        $practica->fill($request->all());
        $practica->fichero = $fichero;
        $practica->save();


        return redirect()
            ->route('practica.show', $practica);
    }

    /**
     * Display the specified resource.
     */
    public function show(Practica $practica)
    {
        return view(
            'practica.show',
            compact('practica')
        );
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Practica $practica)
    {
        // esto es para mostrar un dropdown en curso_id
        $cursos = Curso::all();

        return view(
            'practica.edit',
            compact('practica', 'cursos')
        );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Practica $practica)
    {
        // comprobar si ha subido un nuevo archivo
        if ($request->hasFile('fichero')) {

            // archivo nuevo
            // $request->file('fichero') // objeto

            // archivo antiguo
            // $practica->fichero // string 

            // recupero el archivo
            $ficheroEnviado = $request->file('fichero');

            // elimino el archivo anterior
            Storage::disk('public')->delete($practica->fichero);

            // guardo el nuevo archivo
            $fichero = $ficheroEnviado->store('ficheros', 'public');

            // actualizo el registro
            $practica->fill($request->all());

            // sustituir el nombre del archivo antiguo por el nombre
            // del archivo subido
            $practica->fichero = $fichero;
        } else {
            // si no subo un nuevo archivo
            $practica->fill($request->all());
        }

        $practica->save();

        return redirect()
            ->route('practica.show', $practica);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Practica $practica)
    {
        // elimino el archivo
        Storage::disk('public')->delete($practica->fichero);

        // elimino el registro
        $practica->delete();

        return redirect()
            ->route('practica.index');
    }

    public function confirmar(Practica $practica)
    {
        return view('practica.confirmar', compact('practica'));
    }
}
