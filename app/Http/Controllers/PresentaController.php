<?php

namespace App\Http\Controllers;

use App\Models\Presenta;
use Illuminate\Http\Request;

class PresentaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $presentum = Presenta::all();

        return view(
            'presenta.index',
            compact('presentum')
        );
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Presenta $presentum)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Presenta $presentum)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Presenta $presentum)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Presenta $presentum)
    {
        //
    }
}
