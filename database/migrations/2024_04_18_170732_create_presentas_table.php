<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('presentas', function (Blueprint $table) {
            $table->id();
            $table->float('nota')->nullable();
            // claves ajenas
            $table->unsignedBigInteger('practica_id');
            $table
                ->foreign('practica_id')
                ->references('id')
                ->on('practicas')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->unsignedBigInteger('alumno_id');
            $table
                ->foreign('alumno_id')
                ->references('id')
                ->on('alumnos')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('presentas');
    }
};
