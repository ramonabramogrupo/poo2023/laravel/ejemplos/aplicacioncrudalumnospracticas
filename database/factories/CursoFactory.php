<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Curso>
 */
class CursoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'nombre' => 'Curso ' . fake()->randomElement(['Excel', 'Laravel', 'PHP', 'Word', 'PowerPoint']) . ' ' . fake()->numerify('alpe-###'),
            'duracion' => fake()->numberBetween(1, 9),
            'fechacomienzo' => fake()->date(),
            'observaciones' => fake()->text(),
        ];
    }
}
