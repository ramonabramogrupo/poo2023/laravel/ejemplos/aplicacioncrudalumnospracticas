<?php

namespace Database\Seeders;

use App\Models\Alumno;
use App\Models\Curso;
use App\Models\Pertenece;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PerteneceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($registro = 0; $registro < 10; $registro++) {
            // creo un curso
            $curso = Curso::factory()
                ->create();

            // creo un alumno
            $alumno = Alumno::factory()
                ->create();
            // creo un registro en pertenece
            // que relaciona el alumno creado con el curso
            Pertenece::factory()
                ->for($curso)
                ->for($alumno)
                ->create();
        }
    }
}
