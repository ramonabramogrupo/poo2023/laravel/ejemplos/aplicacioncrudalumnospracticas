<?php

namespace Database\Seeders;

use App\Models\Alumno;
use App\Models\Curso;
use App\Models\Practica;
use App\Models\Presenta;
use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // User::factory(10)->create();

        // User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        // opcion 1
        // sin utilizar los seeeder

        // podemos llamar a las factories desde aqui directamente sin
        // necesidad de seeder

        // Alumno::factory(10)->create();
        // Curso::factory(10)->create();
        // // numero de cursos a crear
        // for ($numeroCurso = 0; $numeroCurso < 10; $numeroCurso++) {
        //     $curso = Curso::factory()->create();

        //     // numero de practicas por curso
        //     $totalPracticas = rand(1, 10); // numero entero aleatorio entre 1 y 10
        //     for ($numeroPracticas = 0; $numeroPracticas < $totalPracticas; $numeroPracticas++)
        //         Practica::factory()
        //             ->for($curso)
        //             ->create();
        // }

        // opcion 2
        // utilizando los seeder

        $this->call([
            AlumnoSeeder::class,
            CursoSeeder::class,
            PracticaSeeder::class,
            PerteneceSeeder::class,
            PresentaSeeder::class
        ]);
    }
}
