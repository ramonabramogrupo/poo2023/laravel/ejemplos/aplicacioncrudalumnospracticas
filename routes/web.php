<?php

use App\Http\Controllers\AlumnoController;
use App\Http\Controllers\CursoController;
use App\Http\Controllers\PerteneceController;
use App\Http\Controllers\PracticaController;
use App\Http\Controllers\PresentaController;
use App\Models\Practica;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('index');
})->name('inicio');

// enrutamiento para controlador alumno
Route::resource('alumno', AlumnoController::class);

// enrutamiento para controlador curso
Route::resource('curso', CursoController::class);

// enrutamiento para todo
Route::resource('practica', PracticaController::class);
Route::resource('presenta', PresentaController::class);
Route::resource('pertenece', PerteneceController::class);

// enrutamiento adicional para practicas
Route::controller(PracticaController::class)->group(function () {
    Route::get('/practica/confirmar/{practica}', 'confirmar')->name('practica.confirmar');
});

// enrutamiento adicional para pertenece
Route::controller(PerteneceController::class)->group(function () {
    Route::get('/pertenece/confirmar/{pertenece}', 'confirmar')->name('pertenece.confirmar');
});
